#!/usr/bin/env python3

import argparse
import errno
import subprocess

from ncu import NCU


def get_argument_parser():
    parser = argparse.ArgumentParser(description="NVIDIA Nsight Compute CLI wrapper")

    class SingleArgumentAction(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            setattr(namespace, self.dest, " ".join(values))

    parser.add_argument(
        "cmd", nargs="*", action=SingleArgumentAction, help="Specify the command line"
    )

    parser.add_argument(
        "-k", "--kernel", type=str, required=False, help="Specify the kernel name"
    )

    parser.add_argument(
        "-m",
        "--mock",
        type=str,
        help="Specify mock input file to use instead of profiling output",
    )

    parser.add_argument(
        "-o", "--output", type=str, help="Write full profiling output to file"
    )

    parser.add_argument(
        "-q",
        "--quiet",
        default=False,
        action="store_true",
        help="Print profiling output only",
    )

    return parser


def print_results(kernel, results):
    print(f">>> {kernel if kernel else 'kernel'}")
    n = max(map(lambda x: len(x), results.keys()))
    for key, value in results.items():
        if isinstance(value, float):
            if float(value) == 0:
                continue
            value = "{:.3f}".format(value)
        key = key.ljust(n, " ")
        print(f"\t{key}: {value}", end="")
        if "runtime" in key:
            print(" ms")
        elif "flops" in key:
            print(" GFLOP/s")
        elif "ops_xu" in key:
            print(" GOP/s")
        elif "ops_tensor" in key:
            print(" TOP/s")
        elif "bandwidth" in key:
            print(" GB/s")
        elif any([x in key for x in ["inst executed", "cycles active", "pct peak"]]):
            print(" %")
        else:
            print()


def run(cmd):
    try:
        output = subprocess.check_output(cmd.split(), universal_newlines=True)
        print(output)
    except subprocess.CalledProcessError:
        print(f"Failed to execute:\n{cmd}")
        exit(errno.EINVAL)


if __name__ == "__main__":
    parser = get_argument_parser()
    args = parser.parse_args()
    if args.kernel:
        kernels = [args.kernel]
        if not args.quiet:
            run(args.cmd)
    else:
        kernels = NCU.find_kernels(args.cmd, quiet=args.quiet)

    for kernel in kernels:
        ncu = NCU(args.cmd, kernel, args.mock)
        result = ncu.parse()
        print_results(kernel, result)
        if args.output:
            open(args.output, "w").close()
            open(args.output, "a").write(ncu.get_output())
        if kernel != kernels[-1]:
            print()
