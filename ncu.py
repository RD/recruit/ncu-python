import errno
import re
import subprocess
import tempfile


class NCU:
    metrics_runtime = ["gpu__time_duration.avg"]
    metrics_dram = ["dram__bytes_read.sum", "dram__bytes_write.sum"]
    metrics_op_count_hp = [
        "sm__sass_thread_inst_executed_op_hadd_pred_on.sum",
        "sm__sass_thread_inst_executed_op_hmul_pred_on.sum",
        "sm__sass_thread_inst_executed_op_hfma_pred_on.sum",
    ]
    metrics_op_count_sp = [
        "sm__sass_thread_inst_executed_op_fadd_pred_on.sum",
        "sm__sass_thread_inst_executed_op_fmul_pred_on.sum",
        "sm__sass_thread_inst_executed_op_ffma_pred_on.sum",
    ]
    metrics_op_count_dp = [
        "sm__sass_thread_inst_executed_op_dadd_pred_on.sum",
        "sm__sass_thread_inst_executed_op_dmul_pred_on.sum",
        "sm__sass_thread_inst_executed_op_dfma_pred_on.sum",
    ]
    metrics_op_count_xu = ["smsp__thread_inst_executed_pipe_xu_pred_on.sum"]

    metrics_dram_pct_of_peak = [
        "gpu__dram_throughput.avg.pct_of_peak_sustained_elapsed"
    ]

    def __get_metrics(self):
        return ",".join(
            self.metrics_runtime
            + self.metrics_dram
            + self.metrics_op_count_hp
            + self.metrics_op_count_sp
            + self.metrics_op_count_dp
            + self.metrics_op_count_xu
            + self.metrics_pipe_inst_executed
            + self.metrics_pipe_cycles_active
            + self.metrics_dram_pct_of_peak
        )

    def __get_mock_output(self):
        return open(self.mock).read()

    def __get_configuration(self):
        pattern = rf"{self.kernel}.+(\(\d+, \d+, \d+\))x(\(\d+, \d+, \d+\))"
        match = re.search(pattern, self.output)
        grid = match.group(1).replace(" ", "")
        block = match.group(2).replace(" ", "")
        return (grid, block)

    def __get_runtime(self):
        metric = self.metrics_runtime[0].replace(".", "\.")
        pattern = rf"{metric}\s+(\w+)\s+(\d+\.\d+)"
        match = re.search(pattern, self.output)
        unit = match.group(1)
        time = float(match.group(2))
        if unit == "second":
            time *= 1e3
        if unit == "usecond":
            time *= 1e-3
        return time  # in ms

    def __get_ops_fp(self, precision):
        metrics = None

        if precision == "hp":
            metrics = self.metrics_op_count_hp
        if precision == "sp":
            metrics = self.metrics_op_count_sp
        if precision == "dp":
            metrics = self.metrics_op_count_dp

        if not metrics:
            raise RuntimeError(
                f"{precision} is not a valid precision (expected hp, sp or dp)"
            )

        flops = 0
        for metric in metrics:
            metric = metric.replace(".", "\.")
            pattern = re.compile(rf"{metric}\s+inst\s+(\d+(,\d+)*)")
            try:
                result = int(re.search(pattern, self.output).group(1).replace(",", ""))
                if "add" in metric or "mul" in metric:
                    flops += result
                elif "fma" in metric:
                    flops += 2 * result
            except:
                pass

        return flops

    def __get_ops_xu(self):
        metric = self.metrics_op_count_xu[0]
        metric = metric.replace(".", "\.")
        pattern = re.compile(rf"{metric}\s+inst\s+(\d+(,\d+)*)")
        result = int(re.search(pattern, self.output).group(1).replace(",", ""))
        return result

    def __get_bandwidth(self, mode):
        metric = None
        if mode == "read":
            metric = self.metrics_dram[0]
            assert "read" in metric
        elif mode == "write":
            metric = self.metrics_dram[1]
            assert "write" in metric

        if not metric:
            raise RuntimeError(f"{mode} is not a valid mode (expected read or write)")

        metric = metric.replace(".", "\.")
        pattern = re.compile(rf"{metric}\s+(\w)?byte\s+(\d+.\d+)")

        result = 0
        try:
            result = re.search(pattern, self.output)
            unit = result.group(1)
            value = result.group(2)
            if "M" == unit:
                unit = 1
            elif "G" == unit:
                unit = 1e3
            elif "K" == unit:
                unit = 1e-3
            elif None == unit:
                unit = 1e-6

            result = unit * float(value)
        except AttributeError:
            pass

        return result

    def __get_pipe_inst_executed(self):
        pattern = re.compile(
            "sm__inst_executed_pipe_(\w+)\.avg\.pct_of_peak_sustained_active\s+%\s+(\d+.\d+)"
        )
        result = re.findall(pattern, self.output)
        result = map(lambda x: (f"inst executed {x[0]}", x[1]), result)
        result = dict(result)
        result = {key: value for key, value in result.items() if float(value) != 0}
        return result

    def __get_pipe_cycles_active(self):
        pattern = re.compile(
            "sm__pipe_(\w+)_cycles_active\.avg\.pct_of_peak_sustained_active\s+\%\s+(\d+.\d+)"
        )
        result = re.findall(pattern, self.output)
        result = map(lambda x: (f"cycles active {x[0]}", x[1]), result)
        result = dict(result)
        result = {key: value for key, value in result.items() if float(value) != 0}
        return result

    def __get_dram_pct_of_peak(self):
        pattern = re.compile(
            "gpu__(\w+)\.\w+\.pct_of_peak_sustained_elapsed\s+\%\s+(\d+.\d+)"
        )
        result = re.findall(pattern, self.output)
        result = map(lambda x: (f"pct peak {x[0]}", x[1]), result)
        return dict(result)

    def __init__(self, cmd, kernel, mock=False):
        self.query_metrics()
        self.cmd = cmd
        self.kernel = kernel
        self.mock = mock
        self.output = None

    @classmethod
    def query_metrics(self):
        cmd = f"ncu --query-metrics"
        try:
            output = subprocess.check_output(cmd.split(), universal_newlines=True)
        except subprocess.CalledProcessError:
            print(f"Failed to execute:\n{cmd}")
            exit(errno.EINVAL)

        pattern = r"(sm__inst_executed_pipe_\w+)"
        result = re.findall(pattern, output)
        self.metrics_pipe_inst_executed = [
            f"{x}.avg.pct_of_peak_sustained_active" for x in result
        ]

        pattern = r"(sm__pipe_\w+_active)"
        result = re.findall(pattern, output)
        self.metrics_pipe_cycles_active = [
            f"{x}.avg.pct_of_peak_sustained_active" for x in result
        ]

    @classmethod
    def find_kernels(self, cmd, quiet=False):
        cmd_output = None
        ncu_output = None

        with tempfile.NamedTemporaryFile(mode="w+b") as tmp:
            cmd = f"ncu --log-file {tmp.name} {cmd}"
            try:
                cmd_output = subprocess.check_output(
                    cmd.split(), universal_newlines=True
                )
                with open(tmp.name) as f:
                    ncu_output = f.read()
            except subprocess.CalledProcessError:
                print(f"Failed to execute:\n{cmd}")
                exit(errno.EINVAL)

        if not quiet:
            print(f"{cmd_output}")

        pattern = r'Profiling "(\w+|_)'
        result = re.findall(pattern, ncu_output)
        result = list(set(result))
        return result

    def run(self):
        if self.mock:
            self.output = self.__get_mock_output()
            return

        metrics = self.__get_metrics()
        kernel = "" if not self.kernel else f" -k {self.kernel}"
        cmd = f"ncu {kernel} -c 1 --clock-control none --metrics {metrics} {self.cmd}"
        try:
            self.output = subprocess.check_output(cmd.split(), universal_newlines=True)
        except subprocess.CalledProcessError:
            print(f"Failed to execute:\n{cmd}")
            exit(errno.EINVAL)

        if self.kernel and not self.kernel in self.output:
            print(f"Could not find kernel '{self.kernel}' in NCU output")
            exit(errno.EINVAL)

    def parse(self):
        if not self.output:
            self.run()

        try:
            grid, block = self.__get_configuration()  # (grid, block)

            runtime = self.__get_runtime()  # ms

            flops_hp = (self.__get_ops_fp("hp") / runtime) * 1e-6  # gops/s
            flops_sp = (self.__get_ops_fp("sp") / runtime) * 1e-6  # gops/s
            flops_dp = (self.__get_ops_fp("dp") / runtime) * 1e-6  # gops/s

            ops_xu = (self.__get_ops_xu() / runtime) * 1e-6  # gops/s

            mbytes_read = self.__get_bandwidth("read")
            mbytes_written = self.__get_bandwidth("write")
            bandwidth_read = 0
            bandwidth_write = 0
            if mbytes_read:
                bandwidth_read = mbytes_read / float(runtime)  # gb/s
            if mbytes_written:
                bandwidth_write = mbytes_written / float(runtime)  # gb/s
            bandwidth_total = bandwidth_read + bandwidth_write

            result = {
                "configuration": f"grid={grid}, block={block}",
                "runtime": runtime,
                "flops_hp": flops_hp,
                "flops_sp": flops_sp,
                "flops_dp": flops_dp,
                "ops_xu": ops_xu,
                "bandwidth_read": bandwidth_read,
                "bandwidth_write": bandwidth_write,
                "bandwidth_total": bandwidth_total,
            }

            result.update(self.__get_pipe_cycles_active())
            result.update(self.__get_pipe_inst_executed())
            result.update(self.__get_dram_pct_of_peak())

            return result
        except Exception as e:
            print("Failed to parse:\n", self.output)
            exit(errno.EINVAL)

    def get_output(self):
        return self.output
